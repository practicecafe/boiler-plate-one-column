<header>
  <div class="container">
    <div class="row">
      <nav class="navbar" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
          <?php /* Primary navigation */
            wp_nav_menu( [
              'menu' => 'primary_navigation',
              'depth' => 2,
              'container' => false,
              'menu_class' => 'nav navbar',
              'walker' => new wp_bootstrap_navwalker()]
            );
          ?>
        </div>
      </nav>
    </div>
  </div>
</header>
