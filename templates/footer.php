<section class="above-footer">
  <div class="container">
    <div class="row">
      <div class="col-sm-4">
        <?php dynamic_sidebar('footer-left') ?>
      </div>
      <div class="col-sm-4">
        <?php dynamic_sidebar('footer-center') ?>
      </div>
      <div class="col-sm-4">
        <?php dynamic_sidebar('footer-right') ?>
      </div>
    </div>
  </div>
</section>
<footer class="content-info">
  <div class="copy">
    <p>&copy; <?php echo date('Y'); ?> <?php bloginfo( 'name' ); ?> | Dental Marketing by <a href="http://practicecafe.com/" target="_blank">Practice Cafe</a></p>
  </div>
</footer>
